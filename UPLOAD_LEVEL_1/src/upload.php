<?php
// Specify the directory where you want to save the uploaded files
$uploadDirectory = 'uploads/';

// Check if the file parameter is set and if there are no errors during the upload
if (isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {
    $uploadedFile = $_FILES['file'];

    // Generate a unique filename to prevent overwriting existing files
    $targetFileName = $uploadDirectory . basename($uploadedFile['name']);

    // Move the uploaded file to the specified directory
    if (move_uploaded_file($uploadedFile['tmp_name'], $targetFileName)) {
        $response = [
            'success' => true,
            'message' => 'File uploaded successfully.',
            'file_path' => $targetFileName,
        ];
    } else {
        $response = [
            'success' => false,
            'message' => 'Failed to move the uploaded file.',
        ];
    }
} else {
    $response = [
        'success' => false,
        'message' => 'Error during file upload.',
    ];
}

// Send JSON response
header('Content-Type: application/json');
echo json_encode($response);
?>
