function uploadFile() {
    var fileInput = document.getElementById('file');
    var file = fileInput.files[0];

    if (!file) {
        alert('Please choose a file to upload.');
        return;
    }

    var formData = new FormData();
    formData.append('file', file);

    fetch('/upload.php', {
        method: 'POST',
        body: formData,
    })
        .then(response => response.json())
        .then(data => {
            alert("upload success");
            fileInput.value = "";
            // console.log('Upload successful:', data);
            // Handle the response as needed
        })
        .catch(error => {
            alert("upload file failed");
            // console.error('Error during upload:', error);
            // Handle errors
        });
}
document.getElementById('uploadBtn').addEventListener('click', function() {
    uploadFile()
});