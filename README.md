<h1>Welcome Lab For 20GIT ATTT</h1>
Run file RunChallenge by command: <b>chmod +x ./RunChallenge && ./RunChallenge</b> <br>
OR <b>docker compose up -d</b> if using Docker in Windows (Host)<br><br>
Await 5-10 minute <br>
Access ip <b>192.168.56.224</b> OR <b>localhost</b> if using Docker in Windows (Host) and port [8081-8086]<br><br>
[+] port 8081: sql injection<br>
[+] port 8082: sql injection<br>
[+] port 8083: sql injection<br>
[+] port 8084: path hidden<br>
[+] port 8085: os command injection<br>
[+] port 8086: file upload vulnerabilities<br>